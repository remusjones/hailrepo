#pragma once
#include <glm\glm.hpp>
#include "Renderer2D.h"
#include "Segment.h"

class SegmentManager
{
public:
	SegmentManager();
	~SegmentManager();

	void update(Segment a_segment, float a_deltatime);

	float m_speed;

};

