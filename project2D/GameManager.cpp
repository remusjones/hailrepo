#include "GameManager.h"
#include "Segment.h"
#include <iostream>
#include <glm\glm.hpp>

GameManager::GameManager(int a_screenWidth, int a_screenHeight, Helicopter& a_heli, aie::Font& a_font)
{
	srand(time(NULL));
	m_screenWidth = a_screenWidth;
	m_screenHeight = a_screenHeight;
	m_timer = 1.5f;
	m_playerRef = &a_heli;
	m_font = &a_font;
}

GameManager::~GameManager()
{

}
void GameManager::Update(float deltaTime)
{
	m_segmentManager.update(deltaTime);
	m_segmentManager.CheckScore(m_playerRef->GetPosv2());
	if (m_collisionManager.checkList(*m_playerRef, m_segmentManager.segmentList) || (m_playerRef->GetPosv2().y > m_screenHeight * 2 || m_playerRef->GetPosv2().y < -m_screenHeight * 2))
	{

		std::cout << "Game Has ended" << std::endl;
		m_segmentManager.segmentList.clear();
		m_segmentManager = SegmentManager();

		m_playerRef->SetPos(glm::vec3(100.0f, m_screenHeight / 2,0));
		m_score = 0;
	}
	
	
	int curscore = 0;
	for (SegmentManager::segmentPair & pair : m_segmentManager.segmentList)
	{
		if (pair.hasScore && !pair.hasCounted)
		{
			m_score += 1;
			pair.hasCounted = true;
		}
	}
	m_currTime += deltaTime;
	if (m_currTime >= m_timer)
		SpawnObject();

}


void GameManager::Draw(aie::Renderer2D& a_renderer)
{
	//a_renderer.drawText(m_font, )
		char score[32];
	sprintf_s(score, 32, "%i", m_score);
	a_renderer.drawText(m_font, score, m_screenWidth / 2.01f, m_screenHeight / 1.1f);
	m_segmentManager.draw(a_renderer);
}  


void GameManager::SpawnObject()
{
	m_segmentManager.spawnPair(m_playerRef->GetPosv2());
	m_currTime = 0.0f;
}

