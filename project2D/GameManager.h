#pragma once
#include <list>
#include <glm/glm.hpp>
#include <random>
#include <time.h>
#include <Renderer2D.h>
#include "Segment.h"
#include "SegmentManager.h"
#include "Helicopter.h"
#include "CollisionManager.h"
class GameManager
{
public:
	// Default Construct	
	GameManager(int a_screenWidth, int a_screenHeight, Helicopter& a_heli, aie::Font& a_font);
	// Deconstructor (Cleanup mem leaks here)
	~GameManager();
	// a general update to handle spawning of segments
	void Update(float deltaTime);
	// the current time for the object to spawn
	float m_currTime = 0.0f;
	// the spawn rate of the segment spawn
	//float m_spawnTime = 10.0f;
	float m_timer = 0.0f;
	void SpawnObject();
	void Draw(aie::Renderer2D& a_renderer);

	// the segment list to handle destruction
private:
	std::list<Segment> m_segmentList;
	
	int m_screenHeight;
	int m_screenWidth;
	//float m_range = 50.0f;
	//float m_segmentWidth = 30.0f;
	SegmentManager m_segmentManager;
	CollisionManager m_collisionManager;
	int m_score = 0;
	int m_highscore;
	Helicopter* m_playerRef;
	aie::Font* m_font;
};

