#include "Segment.h"



Segment::Segment()
{
	m_height = 1.0f;
	m_width = 5.0f;
	m_pos = glm::vec2(5,5);
}

Segment::Segment(float a_height, float a_width, glm::vec2 a_pos, aie::Texture* a_texture)
{
	m_height = a_height;
	m_width = a_width;
	m_pos = a_pos;
	m_texture = a_texture;
	
}

Segment::~Segment()
{
}

void Segment::setHeight(float a_height)
{
	m_height = a_height;
}

float Segment::getHeight()
{
	return m_height;
}

void Segment::setWidth(float a_width)
{
	m_width = a_width;
}

float Segment::getWidth()
{
	return m_width;
}

void Segment::setPos(glm::vec2 a_pos)
{
	m_pos = a_pos;
}

glm::vec2 Segment::getPos()
{
	return m_pos;
}



void Segment::draw(aie::Renderer2D& renderer)
{
	renderer.drawSprite(m_texture, m_pos.x, m_pos.y, m_width, m_height, 0, 0, m_drawLoc.x, m_drawLoc.y);
}
