#pragma once
#include <glm\glm.hpp>
#include "Helicopter.h"
#include "SegmentManager.h"
class CollisionManager
{
public:
	CollisionManager();
	~CollisionManager();

	bool checkList(Helicopter player, std::list<SegmentManager::segmentPair> &segmentList);

	bool AABBcollision(glm::vec2 obj1Pos, float obj1Height, float obj1Width, glm::vec2 obj2Pos, float obj2Height, float obj2Width, bool isTopAnchor);

};

