#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include "Audio.h"
#include "Helicopter.h"
#include "Segment.h"
#include "GameManager.h"

class Application2D : public aie::Application {
public:

	Application2D();
	virtual ~Application2D();
	
	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

protected:

	aie::Renderer2D*	m_2dRenderer;
	aie::Texture*		m_texture;
	aie::Texture*		m_shipTexture;
	aie::Font*			m_font;
	aie::Audio*			m_audio;
	Helicopter*			m_helicopter;
	Segment*			m_segment;
	float m_cameraX, m_cameraY;
	float m_timer;
	GameManager* 		m_gameManager;
};