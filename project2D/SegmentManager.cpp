#include "SegmentManager.h"
#include <iostream>
#include <math.h>


SegmentManager::SegmentManager()
{

	m_speed = 200;
	m_spawnHeight = 0;
	
}


SegmentManager::~SegmentManager()
{
}


void SegmentManager::update( float a_deltatime)
{
	float speed = m_speed * a_deltatime;
	for (segmentPair & pair : segmentList)
	{
		pair.topSegment->setPos(glm::vec2(pair.topSegment->getPos().x - speed, pair.topSegment->getPos().y));
		pair.bottomSegment->setPos(glm::vec2(pair.bottomSegment->getPos().x - speed, pair.bottomSegment->getPos().y));
	}
	deletePair();

}

void SegmentManager::spawnPair(glm::vec2 a_playerPos)
{
	//////////////////////////////
	// LOTS OF MAGIC NUMBERS YO!//
	//////////////////////////////
	// player pos
	float playerHeight = a_playerPos.y;
	// range that it will spawn from the players position
	int range = 150;

	
	// ??
	m_spawnHeight = (float)playerHeight + (rand() % -range);
	
	// if it is close to the upper screen it will add a different range to seem psuedo random
	if (m_spawnHeight > 620)
	{
		float newRange = 620 + rand() % range;
		m_spawnHeight = newRange - gap;
	}
	// again but inversed   
	else if (m_spawnHeight < 120) 
	{
		float newRange = 120 + rand() % range;
		m_spawnHeight = newRange + gap;
	}
										// the math ^  texture  sizeY   sizeX    middle gap
	segmentPair* temp = new segmentPair(m_spawnHeight, nullptr, 10000.0f, width , gap);
	// add to array
	segmentList.push_back(*temp);
	
}

void SegmentManager::deletePair()
{
	if (segmentList.size() < 1)
		return;

	segmentPair& pair = *(segmentList.begin());

	if (pair.topSegment->getPos().x < -width)
	{
		segmentList.pop_front();
	}
}



void SegmentManager::draw(aie::Renderer2D & renderer)
{
	for (segmentPair & pair : segmentList)
	{
		if (&pair == nullptr)
			continue;
		pair.bottomSegment->draw(renderer);
		pair.topSegment->draw(renderer);
	}
}

void SegmentManager::CheckScore(glm::vec2 a_pos)
{
	for (segmentPair & pair : segmentList)
	{
		if (pair.hasScore || pair.hasCounted)
			continue;

		if (pair.topSegment->getPos().x <= a_pos.x)
		{
			pair.hasScore = true;
		}
			
	}
}

