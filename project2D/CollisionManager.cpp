#include "CollisionManager.h"
#include <Texture.h>




CollisionManager::CollisionManager()
{
}


CollisionManager::~CollisionManager()
{
}

bool CollisionManager::checkList(Helicopter player, std::list<SegmentManager::segmentPair> &segmentList)
{
	aie::Texture* playerTexture =  player.getTexture();

	//check full list of segment pairs for collisions.
	for (SegmentManager::segmentPair & pair : segmentList)
	{
		
    		if (AABBcollision(player.GetPosv2(), playerTexture->getHeight(), playerTexture->getWidth(), pair.topSegment->getPos(), pair.topSegment->getHeight(), pair.topSegment->getWidth(), false))
			return true;
   		else if (AABBcollision(player.GetPosv2(), playerTexture->getHeight(), playerTexture->getWidth(), pair.bottomSegment->getPos(), pair.bottomSegment->getHeight(), pair.bottomSegment->getWidth(), true))
			return true;
	}

	return false;
}

bool CollisionManager::AABBcollision(glm::vec2 obj1Pos, float obj1Height, float obj1Width, glm::vec2 obj2Pos, float obj2Height, float obj2Width, bool isTopAnchor)
{
	//OBj 1 Details

    float x1Min = obj1Pos.x;
	float x1Max = obj1Pos.x + obj1Width;
	float y1Min = obj1Pos.y;
	float y1Max = obj1Pos.y + obj1Height;

	//Obj 2 Details

 	float x2Min = obj2Pos.x;
	float x2Max = obj2Pos.x + obj2Width;
	float y2Min;
	float y2Max;
	if (isTopAnchor)
	{
		y2Min = obj2Pos.y - obj2Height;
		y2Max = obj2Pos.y;
	}
	else
	{
		y2Min = obj2Pos.y;
		y2Max = obj2Pos.y + obj2Height;
	}
		

	//if collision detected return true
	//if (x1Max < x2Min || x1Min > x2Max) return true;
	//if (y1Max < y2Min || y1Min > y2Max) return true;

	if (x1Min > x2Max) return false;
	if (x1Max < x2Min) return false;
	if (y1Min > y2Max) return false;
	if (y1Max < y2Min) return false;

 	return true;
}


