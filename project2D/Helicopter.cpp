#include "Helicopter.h"
#include <iostream>




Helicopter::Helicopter(aie::Texture* a_texture)
{
	m_transform = glm::mat3(1);
	// assign texture
	m_texture = a_texture;
	m_transform[2] = glm::vec3(100, 360, 0);
}

void Helicopter::Update(float deltaTime)
{
	aie::Input* input = aie::Input::getInstance();
	// input move
	if (input->isKeyDown(aie::INPUT_KEY_SPACE))
		addForce();
	else
		removeForce();

	// constant move
	Move(deltaTime);
}

void Helicopter::Draw(aie::Renderer2D & renderer)
{
	// draw sprite
	renderer.setUVRect(0,0,1,1);
	//m_2dRenderer->drawSprite(m_shipTexture, 600, 400, 0, 0, m_timer, 1);
	renderer.drawSpriteTransformed3x3(m_texture,glm::value_ptr(m_transform));
}

void Helicopter::Move(float a_deltaTime)
{
	// assign move
	m_transform[2] += currForce * a_deltaTime;
	glm::vec3 pos = m_transform[2];
}

void Helicopter::SetPos(glm::vec3 a_pos)
{
	// assign move
	m_transform[2] = a_pos;
	glm::vec3 pos = m_transform[2];
}

void Helicopter::addForce()
{
	currForce.y += 10.0f;
	if (currForce.y >= 250.0f)
		currForce.y = 250.0f;
}

void Helicopter::removeForce()
{
	currForce.y -= 5.0f;
	if (currForce.y <= -250.0f)
		currForce.y = -250.0f;

}


aie::Texture* Helicopter::getTexture()
{
	return  m_texture;
}

Helicopter::~Helicopter()
{
}
