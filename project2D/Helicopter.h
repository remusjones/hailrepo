#pragma once
#include <glm\glm.hpp>
#include <glm\ext.hpp>
#include <Renderer2D.h>
#include <Input.h>
class Helicopter
{
public:
	Helicopter(aie::Texture* a_texture);
	void Update(float deltaTime);
	void Draw(aie::Renderer2D& renderer);
	void Move(float a_deltaTime);
	void SetPos(glm::vec3 a_pos);
	void addForce();
	void removeForce();
	glm::vec2 GetPosv2() {
		return glm::vec2(m_transform[2]);
	}
	aie::Texture* getTexture();
	~Helicopter();

private:
	glm::vec3 currForce;
	glm::mat3x3 m_transform;
	aie::Texture* m_texture;
	
	
};

