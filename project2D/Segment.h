#pragma once
#include <glm\glm.hpp>
#include "Renderer2D.h"



class Segment
{
public:
	Segment();
	Segment(float a_height, float a_width, glm::vec2 a_pos, aie::Texture* a_texture);
	~Segment();


	void setHeight(float a_height);
	float getHeight();
	void setWidth(float a_width);
	float getWidth();
	void setPos(glm::vec2 a_pos);
	glm::vec2 getPos();
	
	void draw(aie::Renderer2D& renderer);

	void setDrawLocations(glm::vec2& location) { m_drawLoc = location; }
private:
	glm::vec2 m_drawLoc;

	float m_height;
	float m_width;
	glm::vec2 m_pos;
	aie::Texture* m_texture;

};

