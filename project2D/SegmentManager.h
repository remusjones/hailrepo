#pragma once
#include <glm\glm.hpp>
#include "Renderer2D.h"
#include "Segment.h"
#include "Renderer2D.h"
#include <list>


class SegmentManager
{
public:

	struct segmentPair
	{
		Segment* topSegment;
		Segment* bottomSegment;
		bool hasScore = false;
		bool hasCounted = false;
		segmentPair(float m_spawnHeight, aie::Texture* m_texture, float height, float width, float gap)
		{
			topSegment = new Segment(height, width, glm::vec2(1500, m_spawnHeight + gap), m_texture);
			topSegment->setDrawLocations(glm::vec2(0.5f, 0.0f));

			bottomSegment = new Segment(height, width, glm::vec2(1500, m_spawnHeight - gap), m_texture);
			bottomSegment->setDrawLocations(glm::vec2(0.5f, 1.0f));

		}

		~segmentPair()
		{
			delete topSegment;
			delete bottomSegment;
		}

	};

	SegmentManager();
	~SegmentManager();

	void update(float a_deltatime);
	void spawnPair(glm::vec2 a_playerPos);
	void deletePair();
	void draw(aie::Renderer2D& renderer);


	float m_speed; 
	float m_spawnHeight;
	aie::Texture* m_texture;
	float height = 10000;
	float width = 50;
	float gap = 120;
	void CheckScore(glm::vec2 a_pos);

	std::list <segmentPair> segmentList;
	


	



};

